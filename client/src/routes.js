import React from 'react'
import {Switch, Route, Redirect} from 'react-router-dom'
import {LinksPage} from './pages/LinksPage';
import {AdminPage} from "./pages/AdminPage";
import {HomePage} from "./pages/home/HomePage";
import {AuthPage} from "./pages/AuthPage";

export const useRoutes = isAuthenticated => {
    if (isAuthenticated) {
        return (
            <Switch>
                <Route path="/links" exact>
                    <LinksPage />
                </Route>
                <Route path="/admin" exact>
                    <AdminPage />
                </Route>
                <Route path="/" exact>
                    <HomePage />
                </Route>
                <Redirect to="/admin" />
            </Switch>
        )
    }

    return (
        <Switch>
            <Route path="/" exact>
                <HomePage />
            </Route>
            <Route path="/admin" exact>
                <AuthPage />
            </Route>
            <Redirect to="/" />
        </Switch>
    )

}