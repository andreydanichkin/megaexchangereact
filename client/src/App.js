import React from 'react';
import 'materialize-css';
import './App.css';
import {useRoutes} from "./routes";
import {BrowserRouter as Router} from "react-router-dom";
import {AuthContext} from "./context/AuthContext";
import {useAuth} from "./hooks/auth.hook";
// import {Navbar} from "./components/Navbar";

function App() {
    const {login, logout, token, userId} = useAuth()
    const isAuthenticated = !!token
    const routes = useRoutes(isAuthenticated)

  return (
      <AuthContext.Provider value={{
          login, logout, token, userId, isAuthenticated
      }}>
          <Router>
              {/*{isAuthenticated && <Navbar />}*/}
              <div className="">
                  {routes}
              </div>
          </Router>
      </AuthContext.Provider>
  );
}

export default App;
