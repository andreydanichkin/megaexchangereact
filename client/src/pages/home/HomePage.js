import React, {useContext} from 'react'
import s from './Home.module.css'
import phone from '../../static/phone.png'
import telegram from '../../static/telegram.svg'
import viber from '../../static/viber-icon.png'
import whatsup from '../../static/whatsapp-icon.png'
import logo from '../../static/logo.png'

export const HomePage = () => {

    return(
        <div>

            <header className={s.header}>
                <div className={s.header__container}>
                    <div className={s.header__inner}>
                        <div className={s.header__logo}>
                            <img src={logo} alt="Logo"/>
                                <ul className={s.header__nav}>
                                    <li className="nav_link">
                                        <a className={s.address} href="https://goo.gl/maps/3CtvhxvHBVEcFVYh8">
                                            Проспект Науки 29
                                        </a>
                                    </li>
                                    <li className={s.nav_link} className={s.time_work}>
                                        <span>Пн. - Пт. 10:00 - 19:00</span>
                                        <span>Сб. 10:00 - 17:00</span>
                                    </li>
                                </ul>
                        </div>
                        <div>
                            <ul className={s.contacts}>
                                <li className={s.nav_link} className={s.btn}>
                                    <a href="tg://resolve?domain=Mr_Pablo_B ">
                                        <img src={telegram} alt="telegram"/>Написать
                                    </a>
                                </li>
                                <li className={s.nav_link} className={s.icons}>
                                    <a href="https://wa.me/380931940756">
                                        <img src={whatsup} alt="whatsup"/>
                                    </a>
                                </li>
                                <li className={s.nav_link} className={s.icons}>
                                    <a href="https://viber.click/7931940756">
                                        <img src={viber} alt="viber"/>
                                    </a>
                                </li>
                                <li className={s.nav_link} className={s.icons}>
                                    <a href="tel:+380931940756">
                                        <img src={phone} alt="phone"/>
                                    </a>
                                </li>
                            </ul>
                            <div className={s.number}>093-194-07-56</div>
                        </div>
                    </div>
                </div>
            </header>
            <main>
                <section className={s.bg__color}>
                    <div className={s.table__container}>
                        <h2 className={s.title}>Курсы валют</h2>
                        <div className={s.table}>
                            <div className="table_header-time" id="timeNow">3 Сентябрь 17:12</div>
                            <div className="table_header-buy">
                                <p>Покупка</p>
                            </div>
                            <div className="table_header-sale">Продажа</div>
                            <div className={s.table_block_name}>Курс USDT / USD</div>
                            <div className="table_block-buy">27.35</div>
                            <div className="table_block-sale">27.70</div>
                            <div className={s.table_block_name}>Курс BTC / USD</div>
                            <div className="table_block-buy" data-price="btc-buy">10794.37</div>
                            <div className="table_block-sale" data-price="btc-sale">10837.63</div>
                            <div className={s.table_block_name}>Курс ETH / USD</div>
                            <div className="table_block-buy" data-price="eth-buy">411.18</div>
                            <div className="table_block-sale" data-price="eth-sale">412.82</div>
                        </div>
                        <div className={s.notification__container}>
                            <p>Переводы в Китай 0%</p>
                            <p>Переводы swift, sepa</p>
                            <p>Переводы по Украине</p>
                            <p>Переводы по всему миру</p>
                            <p>Пополнение банковских карт Украинских и Российских банков</p>
                        </div>
                    </div>
                </section>
            </main>

        </div>
    )
}