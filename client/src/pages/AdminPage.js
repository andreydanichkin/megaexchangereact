import React from 'react'
import {Navbar} from "../components/Navbar";

export const AdminPage = () => {
    return(
        <div style={{background: 'lightGray'}}>
            <Navbar />
            <h1>Admin Page</h1>
        </div>
    )
}